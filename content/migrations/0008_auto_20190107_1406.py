# -*- coding: utf-8 -*-
# Generated by Django 1.10.9 on 2019-01-07 13:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0007_auto_20190102_1610'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='planemployeework',
            options={'ordering': ['week', 'day__day']},
        ),
        migrations.RemoveField(
            model_name='employee',
            name='work_hour',
        ),
        migrations.AddField(
            model_name='planemployeework',
            name='status',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='planemployeework',
            name='work_hour',
            field=models.TimeField(blank=True, null=True),
        ),
    ]
