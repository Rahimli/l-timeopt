Droplet Name: rengoeringsystem
IP Address: 167.99.43.5
Username: root
Password: v6vaev6a88e380vev41f0d5ca5

ssh timeopt@167.99.43.5
v6vaev6a88e380vev41f0d5ca5

clearclear


adduser timeopt
passwd timeopt
v6vaev6a88e380vev41f0d5ca5


usermod -aG sudo timeopt
su - timeopt
sudo ls -la /root


sudo apt-get update
sudo apt-get install python3-pip python3-dev libpq-dev postgresql postgresql-contrib nginx
sudo apt-get install nginx mysql-server python3-pip python3-dev libmysqlclient-dev ufw

mysql -u root -p
CREATE DATABASE timeopt CHARACTER SET UTF8;
CREATE USER timeoptuser@localhost IDENTIFIED BY 'V6vaev6a88e380vev41f0d5ca5';
GRANT ALL PRIVILEGES ON timeopt.* TO timeoptuser@localhost;
FLUSH PRIVILEGES;
exit


mysqldump -u timeoptuser -p timeopt > /home/timeopt/timeopt_db.sql
mysql -u username -p database_name < /path/to/file.sql

mysql> use timeopt;
mysql> source /home/timeopt/timeopt_db.sql;


sudo -u postgres psql
CREATE DATABASE timeopt_db;
CREATE USER timeoptuser WITH PASSWORD 'v6vaev6a88e380vev41f0d5ca5';
ALTER ROLE timeoptuser SET client_encoding TO 'utf8';
ALTER ROLE timeoptuser SET default_transaction_isolation TO 'read committed';
ALTER ROLE timeoptuser SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE timeopt_db TO timeoptuser;
\q


sudo -H pip3 install --upgrade pip
sudo -H pip3 install virtualenv

mkdir ~/timeopt
cd ~/timeopt
virtualenv -p /usr/bin/python3.5 timeoptenv
source timeoptenv/bin/activate


pip install django mysqlclient
django-admin.py startproject timeopt ~/timeopt
nano ~/timeopt/timeopt/settings.py



DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'timeopt_db',
        'USER': 'timeoptuser',
        'PASSWORD': 'V6vaev6a88e380vev41f0d5ca5',
        'HOST': 'localhost',
        'PORT': '',
    }
}





EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'timeoptimization.0@gmail.com'
EMAIL_HOST_PASSWORD = 'v6vaev6a88e380vev41f0d5ca5'



sudo ufw allow 8000
~/timeopt/manage.py runserver 0.0.0.0:8000

cd ~/timeopt
gunicorn --bind 0.0.0.0:8000 timeopt.wsgi


deactivate
sudo nano /etc/systemd/system/gunicorn.service

[Unit]
Description=gunicorn daemon
After=network.target

[Service]
User=timeopt
Group=www-data
WorkingDirectory=/home/timeopt/timeopt
ExecStart=/home/timeopt/timeopt/timeoptenv/bin/gunicorn --access-logfile - --workers 3 --bind unix:/home/timeopt/timeopt/timeopt.sock timeopt.wsgi:application

[Install]
WantedBy=multi-user.target


sudo systemctl start gunicorn
sudo systemctl enable gunicorn
sudo journalctl -u gunicorn

sudo systemctl daemon-reload
sudo systemctl restart gunicorn

sudo nano /etc/nginx/sites-available/timeopt

server {
    listen 80;
    server_name 167.99.43.5;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /home/timeopt/timeopt;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/home/timeopt/timeopt/timeopt.sock;
    }
}

sudo ln -s /etc/nginx/sites-available/timeopt /etc/nginx/sites-enabled
sudo nginx -t
sudo systemctl restart nginx
sudo ufw delete allow 8000
sudo ufw allow 'Nginx Full'


v6vaev6a88e380vev41f0d5ca5
sudo apt-get install -y erlang
sudo apt-get install rabbitmq-server

systemctl enable rabbitmq-server
systemctl start rabbitmq-server




v6vaev6a88e380vev41f0d5ca5

sudo apt-get install supervisor
sudo nano /etc/supervisor/conf.d/timeopt-celery.conf

[program:timeopt-celery]
command=/home/timeopt/timeopt/timeoptenv/bin/celery worker -A timeopt --loglevel=INFO
directory=/home/timeopt/timeopt
user=nobody
numprocs=1
stdout_logfile=/home/timeopt/logs/celery.log
stderr_logfile=/home/timeopt/logs/celery.log
autostart=true
autorestart=true
startsecs=10

; Need to wait for currently executing tasks to finish at shutdown.
; Increase this if you have very long running tasks.
stopwaitsecs = 600

stopasgroup=true

; Set Celery priority higher than default (999)
; so, if rabbitmq is supervised, it will start first.
priority=1000



sudo supervisorctl reread
sudo supervisorctl update

systemctl status rabbitmq-server


celery -A timeopt worker -l info




v6vaev6a88e380vev41f0d5ca5

sudo systemctl restart gunicorn
sudo systemctl restart nginx

sudo supervisorctl reread
sudo supervisorctl update


django-geoposition==0.3.0




