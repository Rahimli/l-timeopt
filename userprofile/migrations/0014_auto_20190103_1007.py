# -*- coding: utf-8 -*-
# Generated by Django 1.10.9.dev20181113112551 on 2019-01-03 09:07
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0013_auto_20190102_1610'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='key_expires',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2019, 1, 3, 9, 7, 35, 580607, tzinfo=utc), editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='reset_key_expires',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2019, 1, 3, 9, 7, 35, 580648, tzinfo=utc), editable=False, null=True),
        ),
    ]
