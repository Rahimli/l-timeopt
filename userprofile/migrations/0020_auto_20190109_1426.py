# -*- coding: utf-8 -*-
# Generated by Django 1.10.9 on 2019-01-09 13:26
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0019_auto_20190109_1325'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='key_expires',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2019, 1, 9, 13, 26, 26, 980347, tzinfo=utc), editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='reset_key_expires',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2019, 1, 9, 13, 26, 26, 980543, tzinfo=utc), editable=False, null=True),
        ),
    ]
