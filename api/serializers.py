
from django.contrib.auth.models import User
from rest_framework import serializers
from content.models import *


# Serializers define the API representation.
class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = ('id','profile', 'status', 'work_hour', 'first_name', 'last_name', 'image', 'address','email','date')


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ('id','our_company', 'status', 'name', 'minute', 'work_times', 'work_days', 'image','address','position','date',)



class ExcelDocumenSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExcelDocument
        fields = ('excelfile','date',)


