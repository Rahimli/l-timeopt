from django.conf.urls import url, include
from django.contrib.auth.models import User
from rest_framework import serializers, viewsets, routers



# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
from api.views import EmployeeListView, LocationListView, ExcelDocumentView, EmployeeCreateApiView, \
    EmployeeRetrieveAPIView, EmployeeUpdateAPIView, EmployeeDestroyAPIView,LocationCreateApiView, LocationUpdateAPIView,LocationDestroyAPIView, LocationRetrieveAPIView

app_name = 'api'
urlpatterns = [
	url(r'^employees/$', EmployeeListView.as_view(), name='employees-list'),
	url(r'^employees/create/$', EmployeeCreateApiView.as_view(), name='employees-create'),
	url(r'^employees/view/(?P<pk>[0-9]+)/$', EmployeeRetrieveAPIView.as_view(), name='employees-view'),
	url(r'^employees/edit/(?P<pk>[0-9]+)/$', EmployeeUpdateAPIView.as_view(), name='employees-edit'),
	url(r'^employees/remove/(?P<pk>[0-9]+)/$', EmployeeDestroyAPIView.as_view(), name='employees-remove'),


	url(r'^customers/$', LocationListView.as_view(), name='customers'),
	url(r'^customers/create/$', LocationCreateApiView.as_view(), name='customers-create'),
	url(r'^customers/view/(?P<pk>[0-9]+)/$', LocationRetrieveAPIView.as_view(), name='customers-view'),
	url(r'^customers/edit/(?P<pk>[0-9]+)/$', LocationUpdateAPIView.as_view(), name='customers-edit'),
	url(r'^customers/remove/(?P<pk>[0-9]+)/$', LocationDestroyAPIView.as_view(), name='customers-remove'),


	url(r'^excel-documents/$', ExcelDocumentView.as_view(), name='excel-document'),
]